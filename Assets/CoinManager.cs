using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    public CoinBehavior[] Coins;
    public int TotalCoins;
    public int RemainingCoins;

    // Start is called before the first frame update
    void Start()
    {
        Coins = GetComponentsInChildren<CoinBehavior>();
        TotalCoins = Coins.Length;
        RemainingCoins = TotalCoins;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
